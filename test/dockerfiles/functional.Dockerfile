FROM registry.gitlab.com/eyeo/docker/get-browser-binary:node18

ARG SKIP_BUILD=
ARG BROWSER=
COPY .npmrc package*.json webext-sdk/
COPY adblockpluscore/package.json webext-sdk/adblockpluscore/
RUN cd webext-sdk/ && npm ci

COPY . webext-sdk/
WORKDIR webext-sdk/
RUN if [ -z "$SKIP_BUILD" ]; then npm run build; fi
RUN if [ ! -z "$BROWSER" ]; then node test/dockerfiles/get-browser-binaries.js $BROWSER; fi

ENV TEST_PARAMS="3 chromium"
ENV ONLY_FLAKY="false"
ENV TEST_RUNS=1
ENTRYPOINT test/dockerfiles/functional-entrypoint.sh
