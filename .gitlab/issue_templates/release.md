## Background
<!-- Only in case of hotfix, otherwise, remove this section -->

## Release information

- Version: <!-- Specify release version -->
- Milestone: <!-- Link to the Milestone -->

## Steps

Detailed steps: [Release Process](https://gitlab.com/eyeo/adblockplus/abc/webext-ad-filtering-solution/-/wikis/Release-process)

- [ ] (major release only) Run `npm run updatepsl` and commit the result. (ref this issue)
- [ ] Update version in `package.json`, run `npm install`, and commit the result. (ref this issue)
- [ ] Update Release notes
- [ ] Cleanup Milestone issues
- [ ] Tag the version
- [ ] Create next release ticket <!-- Remove in case of the Hotfix -->

/label ~Release
